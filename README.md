# India LFS Users Group Delhi - A branch of the ILUGD group

## Instructions:

* After completing each chapter, write a blog post on [telegra.ph](https://telegra.ph) about the problems you encountered (Not necessary if it was pretty uneventful)
* Push those links into the master under your folder (with your username). So for instance, if your username was USERNAME, you'd create a directory named USERNAME and under that you'll keep all your files
* Have fun!